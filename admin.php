<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<style>
    .form{
        margin: 10px;
        display: inline-block;
    }

    .form input{
        margin: 5px 0;
        display: block;
    }

    .tabs {
        position: relative;
        min-height: 200px; /* This part sucks */
        clear: both;
        margin: 25px 0;
    }
    .tab {
        float: left;
    }
    .tab label {
        background: #eee;
        padding: 10px 20px;
        border: 1px solid #ccc;
        margin-left: -1px;
        position: relative;
        left: 1px;
    }
    .tab label:hover{
        cursor: pointer;
        background-color: #fff200;
    }
    .tab [type=radio] {
        display: none;
    }
    .content {
        height: 800px;
        position: absolute;
        top: 28px;
        left: 0;
        background: white;
        right: 0;
        bottom: 0;
        padding: 20px;
        overflow: hidden;
    }
    [type=radio]:checked ~ label {
        background: white;
        border-bottom: 1px solid white;
        z-index: 2;
    }
    [type=radio]:checked ~ label ~ .content {
        z-index: 1;
    }
    #page-wrap {
        width: 1200px;
        margin: 50px auto;
    }

</style>
<?php
$contents = file_get_contents('test.json');
$obj = json_decode($contents);
$obj->items1[0]->image = 'test';


/*$json = json_encode($obj);
file_put_contents('test.json', $json);*/
?>
<script>
    $(document).ready(function() {
        $.getJSON('test.json', function(data) {
            for(var i = 1; i < 7; i++ ){
                $('#img'+i).attr('src', 'images/' + data.items1[i-1].image + '.jpg');
            }
        });
    });
</script>

<div id="page-wrap">
<div class="tabs">

    <div class="tab">
        <input type="radio" id="tab-1" name="tab-group-1" checked>
        <label for="tab-1">Танк Универсал</label>

        <div class="content">
            <?php
            for($i = 0; $i < 6; $i++) {
                echo '<form class="form">
                    <input type="text" id="image-a' . ($i+1) . '"/>
                    <input type="text" id="model-a' . ($i+1) . '"/>
                    <input type="text" id="user-a' . ($i+1) . '"/>
                    <input type="text" id="sizes-a' . ($i+1) . '"/>
                    <input type="text" id="volume-a' . ($i+1) . '"/>
                    <input type="text" id="weight-a' . ($i+1) . '"/>
                    <input type="text" id="power-a' . ($i+1) . '"/>
                    <input type="text" id="price-a' . ($i+1) . '"/>
                    <input type="text" id="instal-a' . ($i+1) . '"/>
                    <input type="text" id="total-a' . ($i+1) . '"/>
                    <textarea rows="5" cols="22" name="text" id="desc-a' . ($i+1) . '"></textarea>
                    </form>';
            }
            ?>
        </div>
    </div>

    <div class="tab">
        <input type="radio" id="tab-2" name="tab-group-1">
        <label for="tab-2">Дочиста</label>

        <div class="content">
            <p>Stuff for Tab Two</p>

            <img src="http://placekitten.com/200/100">
        </div>
    </div>

    <div class="tab">
        <input type="radio" id="tab-3" name="tab-group-1">
        <label for="tab-3">Термит Профи</label>

        <div class="content">
            <p>Stuff for Tab Three</p>

            <img src="http://placedog.com/200/100">
        </div>
    </div>

    <div class="tab">
        <input type="radio" id="tab-4" name="tab-group-1">
        <label for="tab-4">Биотанк</label>

        <div class="content">
            <p>Stuff for Tab Three</p>

            <img src="http://placedog.com/200/100">
        </div>
    </div>

    <div class="tab">
        <input type="radio" id="tab-5" name="tab-group-1">
        <label for="tab-5">Термит Ergobox</label>

        <div class="content">
            <p>Stuff for Tab Three</p>

            <img src="http://placedog.com/200/100">
        </div>
    </div>

    <div class="tab">
        <input type="radio" id="tab-6" name="tab-group-1">
        <label for="tab-6">Юнилос – Астра</label>

        <div class="content">
            <p>Stuff for Tab Three</p>

            <img src="http://placedog.com/200/100">
        </div>
    </div>

    <div class="tab">
        <input type="radio" id="tab-7" name="tab-group-1">
        <label for="tab-7">Топас</label>

        <div class="content">
            <p>Stuff for Tab Three</p>

            <img src="http://placedog.com/200/100">
        </div>
    </div>

    <div class="tab">
        <input type="radio" id="tab-8" name="tab-group-1">
        <label for="tab-8">Альта- Био</label>

        <div class="content">
            <p>Stuff for Tab Three</p>

            <img src="http://placedog.com/200/100">
        </div>
    </div>

</div>
</div>

