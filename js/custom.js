/* UNKNOWN SH*T */
/*---------------------------------*/
$(function () {
	$('a[href*="#"]:not([href="#"])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});
});

/*---------------------------------*/
/* Resets form and hidden field */
/* after sending */
/*---------------------------------*/

function resetForm() {

	for(var i = 1; i < 6; i++){
		$("#form" + i)[0].reset();
		var hiddenFields = ['fd', 'cpeople', 'cbath', 'cquality', 'cground', 'pres'];
		var i;
		for(i=0; i<6; i++){
			$(hiddenFields[i]).val("");
		}
	}

	var i;
	for(i=1; i<=5; i++){
		$("#form" + i)[0].reset();
	}
}

/*---------------------------------*/
/* AJAX send form */
/*---------------------------------*/

function sendRequest() {
	jQuery.ajax({
		url: "form.php",
		data: 'sname=' + $("#sname").val()
		+ '&sphone=' + $("#sphone").val()
		+ '&fd=' + $("#fd").val()
		+ '&pname=' + $("#pname").val()
		+ '&pphone=' + $("#pphone").val()
		+ '&pres=' + $("#pres").val()
		+ '&tphone=' + $("#tphone").val()
		+ '&fname=' + $("#fname").val()
		+ '&fphone=' + $("#fphone").val()
		+ '&cpeople=' + $("#cpeople").val()
		+ '&cbath=' + $("#cbath").val()
		+ '&cquality=' + $("#cquality").val()
		+ '&cground=' + $("#cground").val()
		+ '&cname=' + $("#cname").val()
		+ '&cphone=' + $("#cphone").val(),
		type: "POST",
        dataType: 'text',
		success: function (data) {
			if(data == 0){
				$("#error-modal").addClass("errshown");
				$("#er-msg").addClass("errshown");
				$("#er-close").addClass("errshown");
			}else if(data == 1){
				$("#sucess-modal").addClass("shown");
				$("#s-msg").addClass("shown");
				$("#s-close").addClass("shown");
			}
			ga('send', 'event', {
			    eventCategory: 'Recieved requests',
			    eventAction: 'sent',
			    eventLabel: 'Successful request'
			  });
			yaCounter39869635.reachGoal('sucessfulRequest');
			resetForm();
		},
		error: function () {
			resetForm();
		}
	});
}

/* Calc form getter */
function fillCalc() {
	var getPeople = $("#quantity1").val();
	var getBath = $("#quantity2").val();
	var getQuality = $('.toggle:checked').val();
	var getGround = $('.toggle2:checked').val();

	$('#cpeople').val(getPeople);
	$('#cbath').val(getBath);
	$('#cquality').val(getQuality);
	$('#cground').val(getGround);
}

/* Tabs */
function openCity(cityName) {
	var i;
	var x = document.getElementsByClassName("tabcontent");
	for (i = 0; i < x.length; i++) {
		x[i].style.display = "none";
	}
	document.getElementById(cityName).style.display = "block";
}
openCity("t1");


$(document).ready(function () {

	//calculator counter
	$('.qtyplus').click(function(e){
		e.preventDefault();
		fieldName = $(this).attr('field');
		var currentVal = parseInt($('input[name='+fieldName+']').val());
		if (!isNaN(currentVal)) {
			$('input[name='+fieldName+']').val(currentVal + 1);
		} else {
			$('input[name='+fieldName+']').val(0);
		}
	});

	$(".qtyminus").click(function(e) {
		e.preventDefault();
		fieldName = $(this).attr('field');
		var currentVal = parseInt($('input[name='+fieldName+']').val());
		if (!isNaN(currentVal) && currentVal > 0) {
			$('input[name='+fieldName+']').val(currentVal - 1);
		} else {
			$('input[name='+fieldName+']').val(0);
		}
	});

	/*$('#first-item').click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#catalog-section").offset().top
		}, 500);
		$('#first-item-tab').addClass('glowing');
		setTimeout(function(){
			$('#first-item-tab').removeClass('glowing');
		},6000);
	});
	$('#second-item').click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#catalog-section").offset().top
		}, 500);
		$('#second-item-tab').addClass('glowing');
		setTimeout(function(){
			$('#second-item-tab').removeClass('glowing');
		},6000);
	});
	$('#third-item').click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#catalog-section").offset().top
		}, 500);
		$('#third-item-tab').addClass('glowing');
		setTimeout(function(){
			$('#third-item-tab').removeClass('glowing');
		},6000);
	});
	$('#fourth-item').click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#catalog-section").offset().top
		}, 500);
		$('#fourth-item-tab').addClass('glowing');
		setTimeout(function(){
			$('#fourth-item-tab').removeClass('glowing');
		},6000);
	});
	$('#fifth-item').click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#catalog-section").offset().top
		}, 500);
		$('#fifth-item-tab').addClass('glowing');
		setTimeout(function(){
			$('#fifth-item-tab').removeClass('glowing');
		},6000);
	});
	$('#sixth-item').click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#catalog-section").offset().top
		}, 500);
		$('#sixth-item-tab').addClass('glowing');
		setTimeout(function(){
			$('#sixth-item-tab').removeClass('glowing');
		},6000);
	});
	$('#seventh-item').click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#catalog-section").offset().top
		}, 500);
		$('#seventh-item-tab').addClass('glowing');
		setTimeout(function(){
			$('#seventh-item-tab').removeClass('glowing');
		},6000);
	});
	$('#eighth-item').click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#catalog-section").offset().top
		}, 500);
		$('#eighth-item-tab').addClass('glowing');
		setTimeout(function(){
			$('#eighth-item-tab').removeClass('glowing');
		},6000);
	});*/

	/* Block not integers in inputs */
	var tels = ["sphone", "cphone", "pphone", "fphone", "tphone"];
	for (var i = 0; i < tels.length; i++) {
		document.getElementById(tels[i]).addEventListener('keydown', function (e) {
			var key = e.keyCode ? e.keyCode : e.which;

			if (!( [8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
					(key == 65 && ( e.ctrlKey || e.metaKey  ) ) ||
					(key >= 35 && key <= 40) ||
					(key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
					(key >= 96 && key <= 105)
				)) e.preventDefault();
		});
	}

	/* Video tabs */
	$('.st1').hover(function () {
		$('.dt1').addClass('hovered')
	}, function () {
		$('.dt1').removeClass('hovered')
	});
	$('.st2').hover(function () {
		$('.dt2').addClass('hovered')
	}, function () {
		$('.dt2').removeClass('hovered')
	});
	$('.st3').hover(function () {
		$('.dt3').addClass('hovered')
	}, function () {
		$('.dt3').removeClass('hovered')
	});
	$('.st4').hover(function () {
		$('.dt4').addClass('hovered')
	}, function () {
		$('.dt4').removeClass('hovered')
	});

	$('#error-modal').click(function(e) {
		if(e.target != document.getElementById('err-msg')) {
			$("#error-modal").removeClass("errshown");
			$("#er-msg").removeClass("errshown");
		}
	});

	$('#sucess-modal').click(function(e) {
		if(e.target != document.getElementById('s-msg')) {
			$("#sucess-modal").removeClass("shown");
			$("#s-msg").removeClass("shown");
		}
	});

	$("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
		e.preventDefault();
		$(this).siblings('a.active').removeClass("active");
		$(this).addClass("active");
		var index = $(this).index();
		$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
		$("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
	});

	$.getJSON('data.json', function(data) {
		var i, k;
		var catsNumber = 8;

		for(i = 0; i < catsNumber; i++){
			var catSize = data.main[i].category.length ;
			switch (i) {
				case 0:
					for( k = 0; k < catSize; k++ ){
						$('#cat0 #img'+(k+1)).attr('src', 'images/septik/' + data.main[0].category[0].item[0].image + '.jpg');
						$('#cat0 #tit'+(k+1)+' h1').text(data.main[0].category[k].item[0].model);
						$('#cat0 #pr'+(k+1)+' h1').text(data.main[0].category[k].item[0].price + ' руб.');
					}
					break;
				case 1:
					for( k = 0; k < catSize; k++ ){
						$('#cat1 #img'+(k+1)).attr('src', 'images/septik/' + data.main[1].category[k].item[0].image + '.jpg');
						$('#cat1 #tit'+(k+1)+' h1').text(data.main[1].category[k].item[0].model);
						$('#cat1 #pr'+(k+1)+' h1').text(data.main[1].category[k].item[0].price + ' руб.');
					}
					break;
				case 2:
					for( k = 0; k < catSize; k++ ){
						$('#cat2 #img'+(k+1)).attr('src', 'images/septik/' + data.main[2].category[k].item[0].image + '.jpg');
						$('#cat2 #tit'+(k+1)+' h1').text(data.main[2].category[k].item[0].model);
						$('#cat2 #pr'+(k+1)+' h1').text(data.main[2].category[k].item[0].price + ' руб.');
					}
					break;
				case 3:
					for( k = 0; k < catSize; k++ ){
						$('#cat3 #img'+(k+1)).attr('src', 'images/septik/' + data.main[3].category[k].item[0].image + '.jpg');
						$('#cat3 #tit'+(k+1)+' h1').text(data.main[3].category[k].item[0].model);
						$('#cat3 #pr'+(k+1)+' h1').text(data.main[3].category[k].item[0].price + ' руб.');
					}
					break;
				case 4:
					for( k = 0; k < catSize; k++ ){
						$('#cat4 #img'+(k+1)).attr('src', 'images/septik/' + data.main[4].category[k].item[0].image + '.jpg');
						$('#cat4 #tit'+(k+1)+' h1').text(data.main[4].category[k].item[0].model);
						$('#cat4 #pr'+(k+1)+' h1').text(data.main[4].category[k].item[0].price + ' руб.');
					}
					break;
				case 5:
					for( k = 0; k < catSize; k++ ){
						$('#cat5 #img'+(k+1)).attr('src', 'images/septik/' + data.main[5].category[k].item[0].image + '.jpg');
						$('#cat5 #tit'+(k+1)+' h1').text(data.main[5].category[k].item[0].model);
						$('#cat5 #pr'+(k+1)+' h1').text(data.main[5].category[k].item[0].price + ' руб.');
					}
					break;
				case 6:
					for( k = 0; k < catSize; k++ ){
						$('#cat6 #img'+(k+1)).attr('src', 'images/septik/' + data.main[6].category[k].item[0].image + '.jpg');
						$('#cat6 #tit'+(k+1)+' h1').text(data.main[6].category[k].item[0].model);
						$('#cat6 #pr'+(k+1)+' h1').text(data.main[6].category[k].item[0].price + ' руб.');
					}
				case 7:
					for( k = 0; k < catSize; k++ ){
						$('#cat7 #img'+(k+1)).attr('src', 'images/septik/' + data.main[7].category[k].item[0].image + '.jpg');
						$('#cat7 #tit'+(k+1)+' h1').text(data.main[7].category[k].item[0].model);
						$('#cat7 #pr'+(k+1)+' h1').text(data.main[7].category[k].item[0].price + ' руб.');
					}
			}
			//console.log("Size of " + i + " category is " + catSize);
		}
	});
});

var sTitle, sPrice, sVolume1, sVolume2, sVybros, sInstall, sWeight, sDesc, sItemName, sFieldId, sArraySize;

function dataExchanger(catId, itemId, recordId, fieldId, itemName) {
	$("#" + fieldId).val(itemName);
	sFieldId = fieldId;

	$.getJSON('data.json', function(data) {
		//prefix m means "Main" element
		mTitle = data.main[catId].category[itemId].item[recordId].model;
		mImage = data.main[catId].category[itemId].item[recordId].image;
		mPrice = data.main[catId].category[itemId].item[recordId].price + " руб.";
		mVolume1 = data.main[catId].category[itemId].item[recordId].volume1;
		mVolume2 = data.main[catId].category[itemId].item[recordId].volume2;
		mVybros = data.main[catId].category[itemId].item[recordId].vybros;
		mInstall = data.main[catId].category[itemId].item[recordId].install;
		mWeight = data.main[catId].category[itemId].item[recordId].weight;
		mDesc = data.main[catId].category[itemId].item[recordId].desc;

		$('#form1 #title h1').text(mTitle);
		$('#form1 #image img').attr('src', 'images/septik/' + mImage + '.jpg');
		$('#form1 #price h2 span').text(mPrice);
		$('#form1 .info #vol1').text(mVolume1);
		$('#form1 .info #vol2').text(mVolume2);
		$('#form1 .info #vybros').text(mVybros);
		$('#form1 .info #install').text(mInstall);
		$('#form1 .info #weight').text(mWeight);

		var itemArray = data.main[catId].category[itemId].item;
		sArraySize = itemArray.length
		htmlStr = "";

		var i;
		for(i = 1; i < sArraySize + 1; i++){
			eval("sTitle" + i + "= data.main[catId].category[itemId].item[recordId+(i-1)].model");
			eval("sPrice" + i + " = data.main[catId].category[itemId].item[recordId+(i-1)].price + ' руб.'");
			eval("sVolume1" + i + " = data.main[catId].category[itemId].item[recordId+(i-1)].volume1");
			eval("sVolume2" + i + " = data.main[catId].category[itemId].item[recordId+(i-1)].volume2");
			eval("sVybros" + i + " = data.main[catId].category[itemId].item[recordId+(i-1)].vybros");
			eval("sInstall" + i + " = data.main[catId].category[itemId].item[recordId+(i-1)].install");
			eval("sWeight" + i + " = data.main[catId].category[itemId].item[recordId+(i-1)].weight");
			eval("sDesc" + i + " = data.main[catId].category[itemId].item[recordId+(i-1)].desc");
			htmlStr += "<input type=\"button\" " +
				"id=\"main-model\" " +
				"class=\"modal-septik-button\" " +
				"value=\"" + data.main[catId].category[itemId].item[recordId+(i-1)].model + "\" " +
				"onclick=\"dataReplacer("+i+")\" />";
		}

		$(".submodels").html(htmlStr);

		if(mDesc.length == 0){
			return false;
		}else{
			$('#form1 .description #desc').text(mDesc);
		}
	});
}

function dataReplacer(id) {
	$("#" + sFieldId).val(eval("sTitle"+id));
	$('#form1 #title h1').text(eval("sTitle"+id));
	$('#form1 #price h2 span').text(eval("sPrice"+id));
	$('#form1 .info #vol1').text(eval("sVolume1"+id));
	$('#form1 .info #vol2').text(eval("sVolume2"+id));
	$('#form1 .info #vybros').text(eval("sVybros"+id));
	$('#form1 .info #install').text(eval("sInstall"+id));
	$('#form1 .info #weight').text(eval("sWeight"+id));
	if(eval("sDesc"+id).length == 0){
		return false;
	}else{
		$('#form1 .description #desc').text(eval("sDesc"+id));
	}

}

$('div#owl-example>div>a').click(function(e) {
	e.preventDefault();

	$('div.bhoechie-tab-menu>div.list-group>a').siblings('a.active').removeClass("active");
	var dataTab = $(this).attr('data-tab');
	$('#' + dataTab).addClass("active");
	$('html, body').animate({
		scrollTop: $('#first-item-tab').offset().top - 100
	}, 500);

	var index = $('#' + dataTab).index();
	$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
	$("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
});

